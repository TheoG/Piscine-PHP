<?php
header('Location: index.html');
function error_exit() {
	echo "ERROR\n";
	exit (1);
}

if (!$_POST["submit"] || !$_POST["login"] || !$_POST["oldpw"] || !$_POST["newpw"]) {
	error_exit();
}

if ($_POST["login"] === "" || $_POST["oldpw"] === "" || $_POST["newpw"] === "" || $_POST["submit"] !== "OK") {
	error_exit();
}

if (!file_exists("/Users/tgros/http/MyWebSite/j04/private") || !file_exists("/Users/tgros/http/MyWebSite/j04/private/passwd")) {
	error_exit();
}

$login = $_POST["login"];
$oldpwd = hash("sha512", $_POST["oldpw"]."ILove42");
$newpwd = hash("sha512", $_POST["newpw"]."ILove42");
$users = unserialize(file_get_contents("/Users/tgros/http/MyWebSite/j04/private/passwd"));
$found = 0;
foreach($users as $key => $user) {
	if ($user["login"] === $login) {
		if ($user["passwd"] === $oldpwd) {
			$users[$key]["passwd"] = $newpwd;
			$found = 1;
			break;
		}
		else {
			error_exit();
		}
	}
}
if ($found == 0)
	error_exit();
file_put_contents("/Users/tgros/http/MyWebSite/j04/private/passwd", serialize($users));
echo "OK\n";
?>