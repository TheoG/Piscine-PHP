<?php
header('Location: index.html');
function error_exit() {
	echo "ERROR\n";
	exit (1);
}

if (!($_POST["submit"]) || !($_POST["login"]) || !($_POST["passwd"])) {
	error_exit();
}
if ($_POST["login"] === "" || $_POST["passwd"] === "" || $_POST["submit"] !== "OK") {
	error_exit();
}

if (!file_exists("/Users/tgros/http/MyWebSite/j04/private")) {
	if (!mkdir("/Users/tgros/http/MyWebSite/j04/private")) {
		error_exit();
	}
}

$login = $_POST["login"];
$pwd = hash("sha512", $_POST["passwd"]."ILove42");
$new_user["login"] = $login;
$new_user["passwd"] = $pwd;

if (file_exists("/Users/tgros/http/MyWebSite/j04/private/passwd")) {
	$users = unserialize(file_get_contents("/Users/tgros/http/MyWebSite/j04/private/passwd"));
}

if (!$users && $users != "") {	
	foreach($users as $user) {
		if ($user["login"] === $login)
			error_exit();
	}
}

$users[] = $new_user;
file_put_contents("/Users/tgros/http/MyWebSite/j04/private/passwd", serialize($users));
echo "OK\n";
?>