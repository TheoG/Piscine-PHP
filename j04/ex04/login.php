<?php

session_start();

include 'auth.php';

if ($_POST["login"] != "" && $_POST["passwd"] != "") {
	if (auth($_POST["login"], $_POST["passwd"]) === FALSE) {
		$_SESSION["logged_on_user"] = "";
		echo "ERROR\n";
	} else {
		$_SESSION["logged_on_user"] = $_POST["login"];
		echo '<html><body>'."\n";
		echo '<iframe name="chat" src="chat.php" height="550px" width="100%"></iframe>'."\n";
		echo '<iframe name="speak" src="speak.php" height="50px" width="100%"></iframe>'."\n";
		echo '<a href="logout.php">Déconnexion</a>'."\n";
		echo '</body></html>'."\n";
	}
} else {
	echo "ERROR\n";
}
?>