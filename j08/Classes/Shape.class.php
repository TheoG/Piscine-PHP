<?php

abstract class Shape {
	private $_pos;
	private $_size; //(array of 4 ints)
	private $_dir; // ()
	private $_color;

	public function __construct(array $kwargs) {
		if (array_key_exists('pos', $kwargs))
			$this->_pos = $kwargs['pos'];
		else
			$this->_pos = array(0, 0);

		if (array_key_exists('size', $kwargs))
			$this->_size = $kwargs['size'];
		else
			$this->_size = array(0, 0, 0, 0);

		if (array_key_exists('dir', $kwargs) && in_array($kwargs['dir'], array('N', 'S', 'E', 'W')))
			$this->_dir = $kwargs['dir'];
		else
			$this->_dir = 'N';

		if (array_key_exists('color', $kwargs))
			$this->_color = $kwargs['color'];
		else
			$this->_color = 0;
	}

	public function testCollision(Shape $shape) {

	}
}

?>