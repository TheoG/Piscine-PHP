<?php

require_once ("Shape.class.php");

class Ship extends Shape {
	private $_name;
	private $_pp;
	private $_life;
	private $_maxlife;
	private $_shield;
	private $_speed;
	private $_movementPts;
	private $_inertia;
	private $_remainingInertia = 0;
	private $_weapons;

	public function __construct(array $kwargs) {
		parent::__construct($kwargs);
		if (array_key_exists('name', $kwargs))
			$this->_name = $kwargs['name'];
		if (array_key_exists('pp', $kwargs))
			$this->_pp = $kwargs['pp'];
		if (array_key_exists('maxlife', $kwargs))
			$this->_maxlife = $kwargs['maxlife'];
		if (array_key_exists('shield', $kwargs))
			$this->_shield = $kwargs['shield'];
		if (array_key_exists('speed', $kwargs))
			$this->_speed = $kwargs['speed'];
		if (array_key_exists('inertia', $kwargs))
			$this->_inertia = $kwargs['inertia'];
		if (array_key_exists('weapons', $kwargs))
			$this->_weapons = $kwargs['weapons'];
		$this->life = $this->_maxlife;
		$this->_movementPts = $this->_speed;
	}

	public function activate () {
		$this->_remainingInertia = $this->_inertia;
	}

	public function move($dir) {


	}

	public function shoot(Weapon $weapon, Ship $target) {

	}

	public function usePP($action) {

	}

	public function getName() {
		return ($this->_name);
	}

	public function getPP() {
		return $this->_pp;
	}

	public function decrementPP() {
		$this->_pp--;
}

}

?>