<?php

require_once ("GameBoard.class.php");
require_once ("Weapon.class.php");
require_once ("Ship.Class.php");
require_once ("Player.class.php");


class Game {
	private $_gameBoard;
	private $_players;
	private $end = FALSE;
	private $turn;

	public function __construct()
	{
		$this->_gameBoard = new GameBoard(150, 100);

		$shipsP1 = $this->_getListShip();
		$shipsP2 = $this->_getListShipP2();

		$this->_players = array(new Player(array("color" => "123456", "name" => "P1")), new Player(array("color" => "654321", "name" => "P2")));

		foreach ($shipsP1 as $ship) {
			$this->_players[0]->addShip($ship);
		}
		foreach ($shipsP2 as $ship) {
			$this->_players[1]->addShip($ship);
		}
		$this->turn = 0;
	}
	public function __destruct() {

	}
	private function _isEndGame() {

	}

	private function _getListShip() {
		$ships;
		$weapons;

		$weapons = array(new Weapon(array("shortRange" => 5, "midRange" => 10, "longRange" => 15)));
		$ships[] = new Ship(array("pos" => array(1, 1), "size" => array(0, 0, 1, 0), "dir" => "W", "color" => "0xFEFEFE", "name" => "Death de la mort", "pp" => 2, "maxlife" => 5, "shield" => "5", "speed" => "5", "inertia" => 0, "weapons" => $weapons));
		return ($ships);
	}

	private function _getListShipP2() {
		$ships;
		$weapons;

		$weapons = array(new Weapon(array("shortRange" => 5, "midRange" => 10, "longRange" => 15)));
		$ships[] = new Ship(array("pos" => array(50, 50), "size" => array(0, 0, 1, 0), "dir" => "W", "color" => "0xFEFEFE", "name" => "Death de la mort", "pp" => 2, "maxlife" => 5, "shield" => "5", "speed" => "5", "inertia" => 0, "weapons" => $weapons));
		return ($ships);
	}

	public function getPlayers() {
		return ($this->_players);
	}


	public function play() {
		// tour a tour

			// get from get =D
			players[$turn]->selectShip($ship);
			players[$turn]->getCurrentShip()->usePP($action);
			players[$turn]->getCurrentShip()->shoot(players[!$turn]->getShips());

			// Tester si tous les beateaux ont ete joues
			// Si oui, recommencer un tour
	}

	public function getGameBoard() {
		return ($this->_gameBoard);
	}

}

?>