<?php


class GameBoard {
	private $_x;
	private $_y;
	private $_board;

	public function __construct($x, $y)
	{
		$this->_x = $x;
		$this->_y = $y;
		$this->_initBoard();
	}

	public function updateObjPos(Shape $shape) {
		$_board[$shape->getPos()[0]][$shape->getPos()[1]] = $shape;
	}

	public function drawBoard() {

	}

	private function _initBoard() {

	}

	public function getBoard() {
		return $this->_board;
	}

	public function getX() {
		return $this->_x;
	}
	public function getY() {
		return $this->_y;
	}
}

?>