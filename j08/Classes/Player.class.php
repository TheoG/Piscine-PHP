<?php

class Player {
	private $_ships;
	private $_color;
	private $_name;
	private $_currentShip;

	public function __construct($kwargs) {
		if (array_key_exists('color', $kwargs))
			$this->_color = $kwargs['color'];
		if (array_key_exists('name', $kwargs))
			$this->_name = $kwargs['name'];
	}

	public function addShip(Ship $ship) {
		$this->_ships[] = $ship;
	}

	public function selectShip(Ship $ship) {
		$this->_currentShip = $ship;
		$ship->activate();
	}

	public function getCurrentShip() {
		return ($this->_currentShip);
	}

	public function getShips() {
		return ($this->_ships);
	}


//	public function play(Ship $ship, $action, $dir, $ennemies) {
//		switch ($action) {
//			case "use PP":
//				$ship->usePP();
//				break;
//			case "move ship" :
//				$ship->move($dir);
//				break;
//			case "shoot":
//				$ship->shoot($ennemies);
//				break;
//		}
//	}
}

?>