<?php

require_once("Classes/Game.class.php");
session_start();

$board = $_SESSION['game']->getGameBoard()->getBoard();
$y = $_SESSION['game']->getGameBoard()->getY();
$x = $_SESSION['game']->getGameBoard()->getX();


?>

<table>
	<?php for($a = 0; $a < $y; $a++) {
		echo "<tr>";
		for ($b = 0; $b < $x; $b++) {
			echo "<td class='box' >".$board[$a][$b]."</td>";
		}
		echo "</tr>";
	}
	?>
</table>
