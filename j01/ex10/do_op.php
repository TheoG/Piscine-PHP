#!/usr/bin/php
<?php

if ($argc != 4)
{
	echo "Incorrect Parameters\n";
	return ;
}

array_shift($argv);
foreach($argv as &$elem)
	$elem = trim($elem);

if ($argv[1] == "+")
	echo $argv[0] + $argv[2]. "\n";
else if ($argv[1] == "-")
	echo $argv[0] - $argv[2]. "\n";
else if ($argv[1] == "*")
	echo $argv[0] * $argv[2]. "\n";
else if ($argv[1] == "/")
	echo $argv[0] / $argv[2]. "\n";
else if ($argv[1] == "%")
	echo $argv[0] % $argv[2]. "\n";
?>