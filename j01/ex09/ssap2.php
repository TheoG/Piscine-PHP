#!/usr/bin/php
<?php

function clean_array($str)
{
	while(false !== strpos($str, '  ')) {
		$str = str_replace('  ', ' ', $str);
	}
	return ($str);
}

function ft_is_char($c1)
{
	return ((ord($c1) >= 97 && ord($c1) <= 122) ||
			(ord($c1) >= 65 && ord($c1) <= 90));
}

function ft_is_upper($c1)
{
	return (ord($c1) >= 65 && ord($c1) <= 90);
}

function cmp_char($c1, $c2)
{
	if (ft_is_char($c1) && ft_is_char($c2))
	{	
		$lower1 = ft_is_upper($c1) ? chr(ord($c1) + 32) : $c1;
		$lower2 = ft_is_upper($c2) ? chr(ord($c2) + 32) : $c2;
		return (ord($lower1) - ord($lower2) > 0);
	}
	if (ft_is_char($c2))
		return (1);
	else if (ft_is_char($c1))
		return (0);
	if (is_numeric($c1) && is_numeric($c2))
	{	
		return ($c1 - $c2 > 0);
	}
	if (is_numeric($c2))
		return (1);
	else if (!is_numeric($c1) && ord($c1) - ord($c2) > 0)
		return (1);
	return (0);
}

function to_lower($a)
{
	if (ord($a) >= 65 && ord($a) <= 90)
		return (ord($a) + 32);
	return (ord($a));
}

function ft_cmp($a, $b)
{
	if ($a == $b)
		return (0);
	for ($i = 0; $i < strlen($a) && $i < strlen($b); $i++)
	{
		if ($a[$i] != $b[$i])
		{
			if (to_lower($a[$i]) == to_lower($b[$i]))
				continue ;
			return (cmp_char($a[$i], $b[$i]));
		}
	}
	if ($i == strlen($b))
		return (1);
	return (0);
}

function bubblesort($tab) {
	$nb_elem = count($tab);
	$is_sorted = 0;
	while (!$is_sorted)
	{
		$is_sorted = 1;
		for ($i = 0; $i < $nb_elem && $is_sorted; $i++)
		{
			for ($j = $i; $j < $nb_elem - 1 - $i && $is_sorted; $j++)
			{
				if (ft_cmp($tab[$j], $tab[$j + 1]))
				{
					$is_sorted = 0;
					$tmp = $tab[$j + 1];
					$tab[$j + 1] = $tab[$j];
					$tab[$j] = $tmp;
				}
			}
		}
	}
	return $tab;
}

if ($argc > 1)
{
	$tab = array();
	array_shift($argv);
	foreach($argv as $argument)
	{
		$value = clean_array(trim($argument));
		$tab = array_merge($tab, explode(" ", $value));
	}
	$tab = bubblesort($tab);
	foreach($tab as $element)
		echo "$element\n";
}

?>