#!/usr/bin/php
<?php

function clean_array($str)
{
	while(false !== strpos($str, '  ')) {
		$str = str_replace('  ', ' ', $str);
	}
	return ($str);
}

if ($argc > 1)
{
	$value = clean_array(trim($argv[1]));
	echo $value;
	echo "\n";
}

?>