#!/usr/bin/php
<?php

$handle = fopen("php://stdin", "r");
if ($handle)
{
	while (!feof($handle))
	{
		echo "Entrez un nombre: ";
		$line = trim(fgets($handle));
		if (feof($handle))
			echo "\n";
		else
		{
			if (is_numeric($line))
			{
				if ($line % 2 == 0)
					echo "Le chiffre " . $line . " est Pair\n";
				else
					echo "Le chiffre " . $line . " est Impair\n";
			}
			else
				echo "'$line' n'est pas un chiffre\n";
		}
	}
	fclose($handle);
}

?>
