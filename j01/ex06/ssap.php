#!/usr/bin/php
<?php

function clean_array($str)
{
	while(false !== strpos($str, '  ')) {
		$str = str_replace('  ', ' ', $str);
	}
	return ($str);
}

if ($argc > 1)
{
	$tab = array();
	array_shift($argv);
	foreach($argv as $argument)
	{
		$value = clean_array(trim($argument));
		$tab = array_merge($tab, explode(" ", $value));
	}
	asort($tab);
	foreach($tab as $element)
		echo "$element\n";
}

?>