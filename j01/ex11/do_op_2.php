#!/usr/bin/php
<?php

function clean_array($str)
{
	$str = str_replace(' ', '', $str);
	return ($str);
}

function trim_numbers(&$number1, &$number2)
{
	$number1 = trim($number1);
	$number2 = trim($number2);
	if (!is_numeric($number1) || !is_numeric($number2))
		error_exit();
}

function error_exit()
{
	echo "Syntax Error\n";
	exit(1);
}

if ($argc != 2)
{
	echo "Incorrect Parameters\n";
	return ;
}
array_shift($argv);
$argv[0] = clean_array($argv[0]);
$nb1 = intval($argv[0]);
if (!is_numeric($nb1))
	error_exit();
$op = substr($argv[0], strlen($nb1), 1);
$nb2 = intval(substr($argv[0], strlen($nb1) + 1));
if (!is_numeric($nb2))
	error_exit();
if ($op == "+")
	echo $nb1 + $nb2. "\n";
else if ($op == "-")
	echo $nb1 - $nb2. "\n";
else if ($op == "*")
	echo $nb1 * $nb2. "\n";
else if ($op == "/" && $nb2 != 0)
	echo $nb1 / $nb2. "\n";
else if ($op == "%" && $nb2 != 0)
	echo $nb1 % $nb2. "\n";
else
	error_exit();
?>