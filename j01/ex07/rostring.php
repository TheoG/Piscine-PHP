#!/usr/bin/php
<?php

function clean_str($str)
{
	while(false !== strpos($str, '  ')) {
		$str = str_replace('  ', ' ', $str);
	}
	$str = 	trim($str);
	return ($str);
}

if ($argc > 1)
{
	$str = clean_str($argv[1]);
	$tab = explode(" ", $str);
	array_push($tab, array_shift($tab));
	echo implode(" ", $tab)."\n";
}

?>