#!/usr/bin/php
<?php

function compute_average($handle, $argv)
{
	$nb_student = 0;
	$somme = 0;
	while (!feof($handle))
	{
		$line = trim(fgets($handle));
		$tab = explode(";", $line);
		if (is_numeric($tab[1]) && $tab[2] != "moulinette")
		{
			$somme += $tab[1];
			$nb_student++;
		}
	}
	echo $somme / $nb_student."\n";
}

function compute_average_per_student($handle, $argv)
{
	$students = array();
	while (!feof($handle))
	{
		$line = trim(fgets($handle));
		$tab = explode(";", $line);
		if (is_numeric($tab[1]) && $tab[2] != "moulinette")
		{
			if (!isset($students[$tab[0]]))
				$students[$tab[0]] = array();
			array_push($students[$tab[0]], $tab[1]);
		}
	}
	ksort($students);
	foreach ($students as $key => $student)
		echo "$key:". array_sum($student) / count($student). "\n";
}

function compute_ecart_moulinette($handle, $argv)
{
	$students = array();
	$students_moulinette = array();
	while (!feof($handle))
	{
		$line = trim(fgets($handle));
		$tab = explode(";", $line);
		if (is_numeric($tab[1]) && $tab[2] != "moulinette")
		{
			if (!isset($students[$tab[0]]))
				$students[$tab[0]] = array();
			array_push($students[$tab[0]], $tab[1]);
		}
		else if (is_numeric($tab[1]) && $tab[2] == "moulinette")
		{
			if (!isset($students_moulinette[$tab[0]]))
				$students_moulinette[$tab[0]] = array();
			array_push($students_moulinette[$tab[0]], $tab[1]);
		}
	}
	ksort($students);
	ksort($students_moulinette);
	foreach ($students as $key => $student)
	{
		$diff = (array_sum($student) / count($student))
			- (array_sum($students_moulinette[$key]) / count($students_moulinette[$key]));
		echo "$key:$diff\n";
	}

}

$handle = fopen("php://stdin", "r");
if ($handle)
{
	if ($argv[1] == "moyenne")
		compute_average($handle, $argv);
	else if ($argv[1] == "moyenne_user")
		compute_average_per_student($handle, $argv);
	else if ($argv[1] == "ecart_moulinette")
		compute_ecart_moulinette($handle, $argv);
	fclose($handle);
}

?>
