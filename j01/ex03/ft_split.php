<?php
function ft_split($array)
{
	$tab = array_filter(array_map('trim', explode(" ", $array)));
	asort($tab);
	return (array_values($tab));
}
?>
