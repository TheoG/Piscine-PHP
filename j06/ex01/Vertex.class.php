<?php

require_once("Color.class.php");

class Vertex
{

	private $_x = 0.0;
	private $_y = 0.0;
	private $_z = 0.0;
	private $_w = 1.0;
	private $_color;
	public static $verbose = FALSE;

	public function __construct(array $kwargs)
	{
		if (array_key_exists('x', $kwargs)) {
			$this->setX(floatval($kwargs['x']));
		}
		if (array_key_exists('y', $kwargs)) {
			$this->setY(floatval($kwargs['y']));
		}
		if (array_key_exists('z', $kwargs)) {
			$this->setZ(floatval($kwargs['z']));
		}
		if (array_key_exists('w', $kwargs)) {
			$this->setW(floatval($kwargs['w']));
		}
		if (array_key_exists('color', $kwargs) && $kwargs['color'] instanceof Color) {
			$this->_color = $kwargs['color'];
		} else {
			$this->_color = new Color(array("red" => "255", "green" => "255", "blue" => "255"));
		}
		if (self::$verbose === TRUE) {
			echo $this->__toString(). " constructed" . PHP_EOL;
		}
	}

	public function __destruct()
	{
		if (self::$verbose === TRUE) {
			echo $this->__toString() . " destructed" . PHP_EOL;
		}
	}

	public function __toString()
	{
		$str = sprintf("Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f", $this->getX(), $this->getY(), $this->getZ(), $this->getW());
		if (self::$verbose === TRUE) {
			$str = $str . ", " . $this->_color;
		}
		$str = $str . " )";
		return $str;
	}

	public static function doc()
	{
		if (file_exists("Vertex.doc.txt")) {
			return (file_get_contents("Vertex.doc.txt") . PHP_EOL);
		}
	}

	public function getX()
	{
		return $this->_x;
	}

	public function setX($x)
	{
		$this->_x = $x;
	}

	public function getY()
	{
		return $this->_y;
	}

	public function setY($y)
	{
		$this->_y = $y;
	}

	public function getZ()
	{
		return $this->_z;
	}

	public function setZ($z)
	{
		$this->_z = $z;
	}

	public function getW()
	{
		return $this->_w;
	}

	public function setW($w)
	{
		$this->_w = $w;
	}

	public function getColor()
	{
		return $this->_color;
	}

	public function setColor($color)
	{
		$this->_color = $color;
	}
}

?>
