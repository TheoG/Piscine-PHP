<?php

class Color {

	public $red 	= 0;
	public $green 	= 0;
	public $blue	= 0;
	public static $verbose = FALSE;

	public function __construct(array $kwargs) {
		if (array_key_exists('rgb', $kwargs)) {
			$this->red 		= intval($kwargs["rgb"]) >> 16;
			$this->green 	= intval($kwargs["rgb"] >> 8 & 0xff);
			$this->blue 	= intval($kwargs["rgb"] & 0xff);
		} else if (array_key_exists('red', $kwargs) && array_key_exists('green', $kwargs) && array_key_exists('blue', $kwargs)) {
			$this->red		= intval($kwargs['red']);
			$this->green	= intval($kwargs["green"]);
			$this->blue		= intval($kwargs["blue"]);
		}
		if (self::$verbose === TRUE) {
			echo $this->__toString(). " constructed.".PHP_EOL;
		}
	}

	public function __destruct() {
		if (self::$verbose === TRUE) {
			echo $this->__toString(). " destructed.".PHP_EOL;
		}
	}

	public function add(Color $rhs) {
		return (new Color(array("red" => ($this->red + $rhs->red), "green" => ($this->green + $rhs->green), "blue" => ($this->blue + $rhs->blue))));
	}

	public function sub(Color $rhs) {
		return (new Color(array("red" => $this->red - $rhs->red, "green" => $this->green - $rhs->green, "blue" => $this->blue - $rhs->blue)));
	}

	public function mult($f) {
		return (new Color(array("red" => $this->red * $f, "green" => $this->green * $f, "blue" => $this->blue * $f)));
	}

	public function __toString() {
		return sprintf("Color( red:%4d, green:%4d, blue:%4d )", $this->red, $this->green, $this->blue);
	}

	public static function doc() {
		if (file_exists("Color.doc.txt"))
			return (file_get_contents("Color.doc.txt") . PHP_EOL);
	}
}

?>
