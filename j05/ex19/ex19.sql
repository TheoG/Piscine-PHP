SELECT DATEDIFF(
	(SELECT MAX(date) FROM historique_membre),
    (SELECT MIN(date) FROM historique_membre)
) as 'uptime';