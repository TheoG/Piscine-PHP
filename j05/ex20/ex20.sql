SELECT DISTINCT f.id_genre as 'id_genre',
				g.nom as 'nom genre',
				d.id_distrib as 'id_distrib',
				d.nom as 'nom distrib',
				titre as 'titre film' FROM film as f 
	LEFT OUTER JOIN genre as g ON f.id_genre = g.id_genre
    LEFT OUTER JOIN distrib as d ON d.id_distrib = f.id_distrib
WHERE f.id_genre >= 4 AND f.id_genre <= 8;