<?php


class NightsWatch {
	private $gardes = array();

	public function recruit($dude) {
		if ($dude instanceof IFighter) {
			$this->gardes[] = $dude;
		}
	}
	public function fight() {
		foreach ($this->gardes as $garde) {
			$garde->fight();
		}
	}
}

?>