<?php

class UnholyFactory {
	private $soldiers = array();

	public function absorb($soldier) {
		if ($soldier instanceof Fighter) {
			if (!in_array($soldier, $this->soldiers)) {
				$this->soldiers[] = $soldier;
				print("(Factory absorbed a fighter of type " . $soldier->getType() . ")\n");
			} else {
				print("(Factory already absorbed a fighter of type " . $soldier->getType() . ")\n");
			}
		} else {
			print("(Factory can't absorb this, it's not a fighter)\n");
		}
	}

	public function fabricate($request) {
		foreach ($this->soldiers as $soldier) {
			if ($soldier->getType() === $request) {
				$str =  ucfirst(str_replace(" ", "", $soldier->getType()));
				print("(Factory fabricates a fighter of type ". $request .")\n");
				return (new $str());
			}
		}
		print("(Factory hasn't absorbed any fighter of type ". $request .")\n");
	}
}

?>