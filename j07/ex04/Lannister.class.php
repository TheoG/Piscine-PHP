<?php

class Lannister {
	const NO = "Not even if I'm drunk !\n";
	const YES = "Let's do this.\n";

	public function sleepWith($dude) {
		if ($dude instanceof Lannister)
			print(self::NO);
		else
			print(self::YES);
	}
}

?>