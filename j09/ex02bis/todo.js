$(document).ready(function() {
	var todo = $('#ft_list');
	elements = document.cookie.split("=")[1];
	if (elements) {
		divs = elements.split("$$$");
		for (var i = 0; i < divs.length; i++) {
			if (divs[i]) {
				var new_div = $("<div>").html(divs[i]);
				new_div.on("click", clickOnElement);
				todo.append(new_div);
			}
		}
	}
});

$("#new-button").on("click", function() {
	var todo = $("#ft_list");
	var new_todo = prompt("Ajouter un élément à la liste : ");
	if (new_todo != null) {
		var new_div = $("<div>").html(new_todo);
		new_div.on("click", clickOnElement);
		todo.prepend(new_div);
		updateCookie();
	}
});

function clickOnElement() {
	var conf = confirm("Voulez-vous vraiment supprimer l'élément '".concat(this.innerHTML).concat("' ?"));
	if (conf == true) {
		console.log(this);
//		$("#ft_list").remove(this);
		this.remove();
		updateCookie();
	}
}

function updateCookie() {
	var elements = $('#ft_list').find('div');
	var cookie = "Todos=";

	console.log(elements);

	for (var i = 0; i < elements.length; i++) {
		cookie = cookie + elements[i].innerHTML + "$$$";
	};
	document.cookie = cookie;
}