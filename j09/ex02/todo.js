window.onload = function() {
	var todo = document.getElementById('ft_list');
	elements = document.cookie.split("=")[1];
	if (elements) {
		divs = elements.split("$$$");
		console.log(divs);
		for (var i = 0; i < divs.length; i++) {
			if (divs[i]) {
				var new_div = document.createElement("div");
				new_div.innerHTML = divs[i];
				new_div.addEventListener("click", clickOnElement, false);
				//			todo.insertBefore(new_div, todo.firstChild);
				todo.appendChild(new_div);
			}
		}
	}
}

document.getElementById('new-button').onclick = function() {
	var todo = document.getElementById('ft_list');
	var new_todo = prompt("Nouvel elem : ");
	if (new_todo != null) {
		var new_div = document.createElement("div");
		new_div.innerHTML = new_todo;
		new_div.addEventListener("click", clickOnElement, false);
		todo.insertBefore(new_div, todo.firstChild);
		updateCookie();
	}
};

function clickOnElement() {
	var conf = confirm("Voulez-vous vraiment supprimer l'élément '".concat(this.innerHTML).concat("' ?"));
	if (conf == true) {
		document.getElementById('ft_list').removeChild(this);
		updateCookie();
	}
}

function updateCookie() {
	var elements = document.getElementById('ft_list').getElementsByTagName('div');
	var cookie = "Todos=";
	for (var i = 0; i < elements.length; i++) {
		cookie = cookie + elements[i].innerHTML + "$$$";
	}
	document.cookie = cookie;
	console.log(document.cookie);
}