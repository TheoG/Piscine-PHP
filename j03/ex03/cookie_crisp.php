<?php

function add_cookie() {
	if ($_GET["name"] && $_GET["value"]) {
		setcookie($_GET["name"], $_GET["value"], time() + 5, "/");
	}
}

function remove_cookie() {
	if ($_GET["name"]) {
		setcookie($_GET["name"], $_GET["value"], time() - 1, "/");
	}
}

function get_cookie() {
	if ($_GET["name"]) {
		if ($_COOKIE[$_GET["name"]])
			echo $_COOKIE[$_GET["name"]]."\n";
	}
}

if ($_GET) {
	if ($_GET["action"]) {
		if ($_GET["action"] == "set") {
			add_cookie();
		} else if ($_GET["action"] == "get") {
			get_cookie();
		} else if ($_GET["action"] == "del") {
			remove_cookie();
		}
	}
}

?>