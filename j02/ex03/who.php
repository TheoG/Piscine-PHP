#!/usr/bin/php
<?php

$str = file_get_contents("/var/run/utmpx");

date_default_timezone_set("Europe/Paris");

$users = substr($str, 1256);

while (is_string($users))
{	
	$login  = substr($users, 0, 32);
	$device  = substr($users, 260, 32);
	$ut_type = substr($users, 296, 4);
	$time = substr($users, 300, 4);
	$device = preg_replace("[\W]", "", $device);
	$login = preg_replace("[\W]", "", $login);

	$time = ((ord($time[3])) << 24) | ((ord($time[2])) << 16) |
		((ord($time[1])) << 8) | (ord($time[0]));
	$ut_type = ((ord($ut_type[3])) << 24) | ((ord($ut_type[2])) << 16) |
		((ord($ut_type[1])) << 8) | (ord($ut_type[0]));

	if ($ut_type == 7)
		printf("%-9s%-9s%s\n", $login, $device, date('M d H:i', $time));
	$users = substr($users, 628);
}
?>