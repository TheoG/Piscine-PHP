#!/usr/bin/php
<?php

function error_exit() {
	echo "Error\n";
	exit (1);
}

if ($argc <= 1)
	exit (1);

if (($res = curl_init($argv[1])) === FALSE)
	error_exit();

curl_setopt($res, CURLOPT_RETURNTRANSFER, TRUE);

if (!$page = curl_exec($res))
	error_exit();

if (mkdir($argv[1]) === FALSE)
	error_exit();

$matches = array();

preg_match_all("#<img.*src=\"(.*?)\".*?>#", $page, $matches);

foreach($matches[1] as $photo) {
	if (!substr($photo, 0, 4) != "http" || !substr($photo, 0, 3) != "www")
	{
		if (substr($photo, 0, 2) == "//")
			$photo = substr($photo, 2, strlen($photo) - 2);
		else if (substr($photo, 0, 1) == "/")
			$photo = $argv[1].$photo;

	}
	if (($image_res = curl_init($photo)) === FALSE)
		continue ;
	curl_setopt($image_res, CURLOPT_RETURNTRANSFER, TRUE);
	file_put_contents($argv[1]."/".end(explode("/", $photo)), curl_exec($image_res));
	curl_close($image_res);
}
curl_close($res);

?>