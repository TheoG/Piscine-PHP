#!/usr/bin/php
<?PHP
function error_exit()
{
	echo ("Wrong Format\n");
	exit(0);
}

date_default_timezone_set("Europe/Paris");
$argv[1] = strtolower($argv[1]);
preg_replace("/é/", "e", $argv[1]);
preg_replace("/û/", "u", $argv[1]);
$days_of_the_week = "lundi|mardi|mercredi|jeudi|vendredi|samedi|dimanche";
$months = "janvier|fevrier|mars|avril|mai|juin|juillet|aout|septembre|octobre|novembre|decembre";
$month_i = array("janvier" => 1, 
				 "fevrier" => 2,
				 "mars" => 3,
				 "avril" => 4,
				 "mai" => 5,
				 "juin" => 6,
				 "juillet" => 7,
				 "aout" => 8,
				 "septembre" => 9,
				 "octobre" => 10,
				 "novembre" => 11,
				 "decembre" => 12);
$check_array = array();
$array = preg_split("/ /", $argv[1]);
if (count($array) != 5)
	error_exit();
if (preg_match("/^".$days_of_the_week."$/", $array[0]) === 0)
	error_exit();
if (preg_match("/^(0[1-9]|[12]\d|3[01])$/", $array[1]) === 0)
	error_exit();
if (preg_match("/^".$months."$/", $array[2]) === 0)
	error_exit();
if (preg_match("/^\d{4}$/", $array[3]) === 0)
	error_exit();
if (preg_match("/^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)/", $array[4]) === 0)
	error_exit();

$time = preg_split("/:/", $array[4]);
echo mktime($time[0], $time[1], $time[2], $month_i[$array[2]], $array[1], $array[3])."\n";

?>