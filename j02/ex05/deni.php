#!/usr/bin/php
<?php

if ($argc != 3) {
	exit (1);
}

$handle = fopen($argv[1], "r"); 

$header = fgetcsv($handle, 0, ';');
for ($i = 0; $i < count($header); $i++)
{
	if ($header[$i] === $argv[2])
	{
		$index = $i;
		break ;
	}
}
if (!isset($index))
	exit (1);
foreach ($header as $i) {
	$$i = array();
}
while (($line = fgetcsv($handle, 0, ';')) !== FALSE) {
	if (count($line) != count($header))
		continue ;
	for ($i = 0; $i < count($header); $i++) {
		${$header[$i]}[$line[$index]] = $line[$i];
	}
}
$handle = fopen("php://stdin", "r");
if ($handle) {
	while (!feof($handle)) {
		echo "Entrez votre commande: ";
		$line = trim(fgets($handle));
		if (feof($handle))
			echo "\n";
		else {
			if (strstr($line, "echo") === FALSE && strstr($line, "print_r") === FALSE)
				echo "PHP Parse error:  syntax error, unexpected T_STRING in [....]\n";
			else
				$str = eval($line)."\n";
		}
	}
	fclose($handle);
}
?>
