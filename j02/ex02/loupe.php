#!/usr/bin/php
<?PHP
function error_exit() {
	exit(0);
}

if (!file_exists($argv[1]))
	error_exit();
$str = file_get_contents($argv[1]);
$str = preg_replace_callback("#(<a )(.*?)(>)(.*)(</a>)#si", f1, $str);
print($str);

function f1($matches) {
	$matches[0] = preg_replace_callback("#( title=\")(.*?)(\")#mi", f2, $matches[0]);
	$matches[0] = preg_replace_callback("#(>)(.*?)(<)#si", f2, $matches[0]);
	return ($matches[0]);
}
function f2($matches) {
	return ($matches[1].strtoupper($matches[2]).$matches[3]);
}
?>